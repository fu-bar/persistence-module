package osf.persistence.communication.rest;

import io.dropwizard.Application;
import io.dropwizard.setup.Environment;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import osf.persistence.config.Config;
import osf.persistence.utilities.Utils;

public class RESTServer extends Application<Config> {
    private static final Logger log = LogManager.getLogger(RESTServer.class);
    private Config configuration;

    @Override
    public void run(Config configuration, Environment environment) {
        this.configuration = configuration;
        Utils.registerDummyHealthCheck(environment);
        environment.jersey().register(new RequestHandler(configuration));
    }

    public Config getConfiguration() {
        return configuration;
    }
}
