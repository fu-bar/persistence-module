package osf.persistence.communication.rest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import osf.persistence.config.Config;
import osf.persistence.plugins.base.OSFPersistencePlugin;
import osf.persistence.structures.Query;
import osf.persistence.structures.Result;
import osf.persistence.utilities.Utils;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Base64;
import java.util.List;
import java.util.Map;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public class RequestHandler {
    private static final String PAYLOAD_KEY = "payload";
    private static final String METADATA_KEY = "metadata";
    private static final String MESSAGE_KEY = "message";
    private final Logger log = LogManager.getLogger(RequestHandler.class);
    private OSFPersistencePlugin persistencePlugin;

    public RequestHandler(Config config) {
        try {
            Constructor<?> pluginConstructor = config.getPersistencePluginClass().getConstructor(Config.class);
            this.persistencePlugin = (OSFPersistencePlugin) pluginConstructor.newInstance(config);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            String errorMessage = "Unexpected error while instantiating persistence plugin";
            log.error(errorMessage, e);
            Utils.abort(errorMessage, e);
        }
    }

    @POST
    @Path("/store")
    public Response store(Map<String, Object> data) {
        if (!data.containsKey(PAYLOAD_KEY)) {
            return Response.serverError().entity(Map.of(MESSAGE_KEY, "payload key not found")).build();
        }
        String payloadString = (String) data.get(PAYLOAD_KEY);
        byte[] payload = Base64.getDecoder().decode(payloadString);
        data.remove(PAYLOAD_KEY); // Data without the payload leaves only the metadata
        boolean saved = this.persistencePlugin.store(data, payload);
        if (!saved) {
            return Response.serverError().entity(Map.of(MESSAGE_KEY, "failed persisting payload")).build();
        }
        return Response.ok().build();
    }

    @POST
    @Path("/query")
    public Response query(Map<String, Object> metadata) {
        Query query = Utils.queryFromMap(metadata);
        List<Result> results = this.persistencePlugin.query(query);
        return Response.ok().entity(results).build();
    }
}
