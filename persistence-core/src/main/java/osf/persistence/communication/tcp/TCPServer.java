package osf.persistence.communication.tcp;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import osf.persistence.config.Config;
import osf.persistence.plugins.base.OSFPersistencePlugin;
import osf.persistence.utilities.Utils;
import osf.shared.dev.SafeRunnable;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.function.Consumer;

public class TCPServer {
    private final Logger log = LogManager.getLogger(TCPServer.class);
    private final Consumer<Throwable> errorHandler;
    private final Constructor<?> pluginConstructor;
    private final Config config;
    private ServerSocket serverSocket;
    private boolean openForBusiness = true;

    public TCPServer(Config config, Consumer<Throwable> errorHandler) throws NoSuchMethodException {
        this.config = config;
        this.errorHandler = errorHandler;
        Class<?> pluginClass = config.getPersistencePluginClass();
        this.pluginConstructor = pluginClass.getConstructor(Config.class);
        log.debug("Persistence plugin: {}", config.getPersistencePluginClass().getCanonicalName());
    }

    public void start() {
        int tcpServerPort = config.getTcpServerPort();
        try {
            serverSocket = new ServerSocket(tcpServerPort);
            log.info("TCP server is listening on port: {}", tcpServerPort);
        } catch (IOException e) {
            Utils.abort("Failed initiating TCP server", e);
        }
        while (openForBusiness) {
            Socket clientSocket;
            try {
                clientSocket = serverSocket.accept();
            } catch (IOException e) {
                log.info("Server socket closed");
                this.openForBusiness = false;
                continue;
            }
            log.debug("Received connection from: {}:{}", clientSocket.getInetAddress(), clientSocket.getPort());
            OSFPersistencePlugin pluginInstance = null;
            try {
                pluginInstance = (OSFPersistencePlugin) this.pluginConstructor.newInstance(config);
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
                String errorMessage = "Unexpected error while instantiating persistence plugin";
                log.error(errorMessage, e);
                Utils.abort(errorMessage, e);
            }
            SafeRunnable safeRunnable = new SafeRunnable(
                    new ClientHandler(clientSocket, pluginInstance),
                    this.errorHandler
            );

            Thread thread = new Thread(safeRunnable);
            String threadName = String.format("%s:%d", clientSocket.getInetAddress(), clientSocket.getPort());
            thread.setName(threadName);
            thread.setDaemon(true);
            thread.start();
        }
        log.info("Goodbye");
    }

    public void stop() throws IOException {
        this.serverSocket.close();
    }
}
