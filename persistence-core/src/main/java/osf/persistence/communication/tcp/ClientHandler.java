package osf.persistence.communication.tcp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import osf.persistence.exceptions.PersistenceException;
import osf.persistence.plugins.base.OSFPersistencePlugin;
import osf.persistence.structures.Query;
import osf.persistence.structures.Result;
import osf.persistence.utilities.Utils;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.List;
import java.util.Map;

import static osf.persistence.communication.tcp.Protocol.*;

public class ClientHandler implements Runnable {

    private final Socket clientSocket;
    private final Logger log = LogManager.getLogger(ClientHandler.class);
    private final BufferedInputStream inputStream;
    private final OutputStream outputStream;
    private final OSFPersistencePlugin persistencePlugin;
    private boolean isListening = true;

    public ClientHandler(Socket clientSocket, OSFPersistencePlugin persistencePlugin) {
        this.clientSocket = clientSocket;
        this.persistencePlugin = persistencePlugin;
        try {
            this.inputStream = new BufferedInputStream(clientSocket.getInputStream());
            this.outputStream = clientSocket.getOutputStream();
        } catch (IOException e) {
            String errorMessage = String.format("Failed connecting with: %s", clientSocket.getInetAddress());
            log.error(errorMessage, e);
            throw new PersistenceException(errorMessage, e);
        }
    }

    @Override
    public void run() {
        while (isListening) {
            try {
                byte[] magicNumber = inputStream.readNBytes(1);
                if (magicNumber.length == 0) {
                    log.debug("Client disconnected");
                    this.isListening = false;
                    continue;
                }
                switch (magicNumber[0]) {
                    case STORE_MAGIC:
                        String metadataString = Protocol.readMetadata(inputStream);
                        byte[] payloadBytes = Protocol.readPayload(inputStream);
                        this.store(metadataString, payloadBytes);
                        break;
                    case QUERY_MAGIC:
                        metadataString = Protocol.readMetadata(inputStream);
                        this.query(metadataString);
                        break;
                    case GOODBYE_MAGIC:
                        readSizedBytes(inputStream, true);
                        this.isListening = false;
                        log.debug("Ending communication with {}:{}", clientSocket.getRemoteSocketAddress(), clientSocket.getPort());
                        return;
                    default:
                        String errorMessage = String.format("Unexpected opcode: %d (expected one of: %d, %d, %d)",
                                magicNumber[0],
                                STORE_MAGIC,
                                QUERY_MAGIC,
                                GOODBYE_MAGIC);
                        this.isListening = false;
                        Protocol.sendError(errorMessage, clientSocket);

                }
            } catch (IOException e) {
                this.isListening = false;
                if (e.getMessage().equals("Connection reset")) {
                    log.warn("Client {}:{} disconnected without closing the socket", clientSocket.getInetAddress(), clientSocket.getPort());
                    continue;
                }
                log.error("Unexpected exception while reading from: {}", clientSocket.getInetAddress(), e);
                try {
                    this.outputStream.write(ERROR_MAGIC);
                } catch (IOException ex) {
                    log.error("Unexpected exception while writing error response to: {}", clientSocket.getInetAddress(), e);
                }
            }
        }
    }

    private void query(String metadataString) {
        try {
            Map<String, Object> metadata = Utils.OBJECT_MAPPER.readValue(metadataString, new TypeReference<>() {
            });
            Query query = Utils.queryFromMap(metadata);
            List<Result> results = this.persistencePlugin.query(query);
            Protocol.sendQueryResults(results, outputStream);
        } catch (JsonProcessingException e) {
            log.error("Failed parsing metadata: {}", metadataString, e);
            Protocol.sendError("Failed parsing metadata string", clientSocket);
        }
    }

    private void store(String metadataString, byte[] payload) {
        try {
            Map<String, Object> metadata = Utils.OBJECT_MAPPER.readValue(metadataString, new TypeReference<>() {
            });
            boolean success = this.persistencePlugin.store(metadata, payload);
            if (success) {
                Protocol.sendSuccess(outputStream);
            } else {
                Protocol.sendError("Failed storing " + Utils.toJson(metadata), clientSocket);
            }
        } catch (JsonProcessingException e) {
            log.error("Failed parsing metadata: {}", metadataString, e);
            Protocol.sendError("Failed parsing metadata string", clientSocket);
        } catch (IOException e) {
            log.error("Failed responding to client", e);
        }
    }
}
