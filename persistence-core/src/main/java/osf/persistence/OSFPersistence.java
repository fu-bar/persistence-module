package osf.persistence;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import osf.persistence.communication.rest.RESTServer;
import osf.persistence.communication.tcp.TCPServer;
import osf.persistence.config.Config;
import osf.persistence.utilities.Utils;

public class OSFPersistence {
    private static final Logger log = LogManager.getLogger(OSFPersistence.class);

    public static void main(String[] args) throws Exception {
        String configFilePath;
        if (args.length > 0) {
            configFilePath = args[0];
            log.debug("Configuration file is set to: {}", configFilePath);
        } else {
            configFilePath = "config.json";
            log.warn("Configuration file was not supplied in command line, defaulting to: {}", configFilePath);
        }
        if (!Utils.fileExists(configFilePath)) {
            Utils.abort(String.format("Configuration file not found: %s", configFilePath), null);
        }

        Utils.printLogo();

        RESTServer restServer = new RESTServer();
        log.info("Configuration file: {}", configFilePath);
        restServer.run("server", configFilePath);
        Config config = restServer.getConfiguration();
        TCPServer tcpServer = new TCPServer(config, throwable -> log.error("Unexpected error: {}", throwable.getMessage(), throwable));
        new Thread(tcpServer::start).start();
    }


}
