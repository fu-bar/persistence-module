package sandbox;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.Map;

public class TCPApp {
    private static final Logger log = LogManager.getLogger(TCPApp.class);
    private static final String GOODBYE_STRING = "goodbye";
    private static String input = "";

    public static void main(String[] args) throws IOException {
        TCPClient tcpClient = new TCPClient("localhost", 2104);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        log.debug("Type something and press enter to transmit it (type \"{}\" to disconnect)", GOODBYE_STRING);
        while (!input.equals(GOODBYE_STRING)) {
            input = bufferedReader.readLine();
            if (input.equals(GOODBYE_STRING)) {
                continue;
            }
            if (input.equals("auto")) {
                for (int i = 0; i < 10; i++) {
                    String input = "Auto-Input-" + i;
                    log.debug("Storing: {}", input);
                    tcpClient.store(getMetadata(), input.getBytes(StandardCharsets.UTF_8));
                }
                tcpClient.disconnect();
                log.debug("Bye!");
                return;
            }
            tcpClient.store(getMetadata(), input.getBytes(StandardCharsets.UTF_8));
        }
        tcpClient.disconnect();
        log.debug("Adios!");
    }

    private static Map<String, Object> getMetadata() {
        return Map.of("timestamp", Instant.now().getEpochSecond());
    }
}
