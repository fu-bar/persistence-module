package sandbox;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import osf.persistence.communication.tcp.Protocol;
import osf.persistence.utilities.Utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import static osf.persistence.communication.tcp.Protocol.STORE_MAGIC;
import static osf.persistence.communication.tcp.Protocol.validateSuccess;

public class TCPClient {
    private static final Logger log = LogManager.getLogger(TCPClient.class);
    private final Socket clientSocket;
    private final InputStream inputStream;
    private final OutputStream outputStream;
    private static final int SOCKET_TIMEOUT_MILLISECONDS = 5000;

    public TCPClient(String host, int port) throws IOException {
        log.debug("Starting a client on host {} port {}", host, port);
        this.clientSocket = new Socket(InetAddress.getByName(host), port);
        this.clientSocket.setSoTimeout(SOCKET_TIMEOUT_MILLISECONDS);
        this.outputStream = clientSocket.getOutputStream();
        this.inputStream = clientSocket.getInputStream();
    }

    public void store(Map<String, Object> metadata, byte[] payload) {
        try {
            outputStream.write(STORE_MAGIC);
            Protocol.sendSizedBytes(Utils.toJson(metadata).getBytes(StandardCharsets.UTF_8), outputStream, true);
            Protocol.sendSizedBytes(payload, outputStream, true);
            validateSuccess(inputStream);
        } catch (IOException e) {
            log.error("IO Exception while storing", e);
        }
    }

    public void disconnect() {
        Protocol.disconnect(clientSocket);
    }

}
