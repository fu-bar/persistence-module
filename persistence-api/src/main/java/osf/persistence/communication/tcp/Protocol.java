package osf.persistence.communication.tcp;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import osf.persistence.exceptions.CommunicationException;
import osf.persistence.exceptions.ProtocolException;
import osf.persistence.structures.Result;
import osf.persistence.utilities.Utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static osf.persistence.utilities.Utils.bytesToInteger;

public class Protocol {
    public static final byte STORE_MAGIC = 0;
    public static final byte QUERY_MAGIC = 1;
    public static final byte SUCCESS_MAGIC = 2;
    public static final byte ERROR_MAGIC = 3;
    public static final byte REPLY_MAGIC = 4;
    public static final byte GOODBYE_MAGIC = 5;
    public static final byte PERIOD_MAGIC = 7;
    private static final Logger log = LogManager.getLogger(Protocol.class);
    private static final int SIZE_OF_INT = 4;

    private Protocol() {
        // Static
    }

    public static String readMetadata(InputStream inputStream) {
        byte[] metadataBytes = readSizedBytes(inputStream, true);
        return new String(metadataBytes);
    }

    public static byte[] readPayload(InputStream inputStream) {
        return readSizedBytes(inputStream, true);
    }

    public static void sendQueryResults(List<Result> results, OutputStream outputStream) {
        try {
            outputStream.write(REPLY_MAGIC);
            ByteBuffer countResultsBuffer = ByteBuffer.allocate(SIZE_OF_INT);
            countResultsBuffer.putInt(results.size());
            outputStream.write(countResultsBuffer.array());
            for (var result : results) {
                String resultMetadata = Utils.toJson(result.getMetadata());
                sendSizedBytes(resultMetadata.getBytes(StandardCharsets.UTF_8), outputStream, false);
                sendSizedBytes(result.getPayload(), outputStream, true);
            }
            outputStream.write(PERIOD_MAGIC);
            outputStream.flush();
        } catch (IOException e) {
            log.error("Failed sending results to client", e);
            throw new CommunicationException("Failed sending results to client", e);
        }
    }

    public static byte[] readSizedBytes(InputStream inputStream, boolean expectPeriod) {
        try {
            byte[] payloadSizeBytes = inputStream.readNBytes(SIZE_OF_INT);
            int payloadSize = Utils.bytesToInteger(payloadSizeBytes);
            byte[] bytes = inputStream.readNBytes(payloadSize);
            if (expectPeriod) {
                validatePeriod(inputStream);
            }
            return bytes;
        } catch (IOException e) {
            throw new CommunicationException("Failed reading sized bytes");
        }
    }

    public static void sendSizedBytes(byte[] bytes, OutputStream outputStream, boolean addPeriod) {
        ByteBuffer sizeBuffer = ByteBuffer.allocate(SIZE_OF_INT);
        sizeBuffer.putInt(bytes.length);
        try {
            outputStream.write(sizeBuffer.array());
            outputStream.write(bytes);
            if (addPeriod) {
                outputStream.write(PERIOD_MAGIC);
            }
            outputStream.flush();
        } catch (IOException e) {
            log.error("Failed sending {} bytes.", bytes.length);
            throw new CommunicationException("Failed sending sized byte-array", e);
        }
    }

    public static void disconnect(Socket clientSocket) {
        log.debug("Disconnecting from {}", clientSocket.getRemoteSocketAddress());
        OutputStream outputStream;
        try {
            outputStream = clientSocket.getOutputStream();
        } catch (IOException e) {
            throw new CommunicationException("Failed getting an output stream from client socket", e);
        }
        try {
            outputStream.write(GOODBYE_MAGIC);
            ByteBuffer sizeBuffer = ByteBuffer.allocate(SIZE_OF_INT);
            sizeBuffer.putInt(0);
            outputStream.write(sizeBuffer.array());
            outputStream.write(PERIOD_MAGIC);
            outputStream.flush();
            clientSocket.close();
        } catch (IOException e) {
            log.error("Failed properly disconnecting from {}", clientSocket.getRemoteSocketAddress(), e);
            throw new CommunicationException("Proper disconnection failed", e);
        }
    }

    public static void sendSuccess(OutputStream outputStream) {
        try {
            outputStream.write(new byte[]{SUCCESS_MAGIC, PERIOD_MAGIC});
            outputStream.flush();
        } catch (IOException e) {
            log.error("Failed reporting success to client", e);
        }
    }

    public static void sendError(String errorMessage, Socket clientSocket) {
        try {
            OutputStream outputStream = clientSocket.getOutputStream();
            outputStream.write(ERROR_MAGIC);
            byte[] messageBytes = errorMessage.getBytes(StandardCharsets.UTF_8);
            sendSizedBytes(messageBytes, outputStream, true);
            log.debug("Closing client socket");
            clientSocket.close();
        } catch (IOException e) {
            log.error("Failed delivering an error message to the client", e);
        }
    }

    static void validatePeriod(InputStream inputStream) {
        try {
            byte[] mustBePeriod = inputStream.readNBytes(1);
            if (mustBePeriod[0] != PERIOD_MAGIC) {
                log.error("Expected content to be followed by a period byte ({})", PERIOD_MAGIC);
                throw new ProtocolException("Expected content to be followed by a period byte");
            }
        } catch (IOException e) {
            log.error("Failed reading from input-stream", e);
            throw new CommunicationException("Failed reading from input-stream", e);
        }
    }

    public static void validateSuccess(InputStream inputStream) throws IOException {
        byte[] bytes = inputStream.readNBytes(1);
        byte responseByte = bytes[0];
        switch (responseByte) {
            case SUCCESS_MAGIC:
                validatePeriod(inputStream);
                return;
            case ERROR_MAGIC:
                byte[] payloadSizeBytes = inputStream.readNBytes(4);
                int errorMessageSize = bytesToInteger(payloadSizeBytes);
                String errorMessage;
                if (errorMessageSize > 0) {
                    byte[] errorMessageBytes = inputStream.readNBytes(errorMessageSize);
                    errorMessage = new String(errorMessageBytes);
                    log.error(errorMessage);
                } else {
                    errorMessage = "Error message not provided (0 size)";
                }
                validatePeriod(inputStream);
                throw new ProtocolException(errorMessage);
            default:
                throw new ProtocolException(String.format("Unexpected read byte %d, expected %d or %d", responseByte, SUCCESS_MAGIC, ERROR_MAGIC));
        }
    }
}
