package osf.persistence.plugins;

import osf.persistence.config.Config;
import osf.persistence.plugins.base.OSFPersistencePlugin;
import osf.persistence.structures.Query;
import osf.persistence.structures.Result;

import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

public class DummyPlugin extends OSFPersistencePlugin {
    private final CharsetDecoder decoder = StandardCharsets.UTF_8.newDecoder();

    public DummyPlugin(Config configuration) {
        super(configuration);
    }

    @Override
    public boolean store(Map<String, Object> metadata, byte[] payloadBytes) {
        String string = fromUTF8(payloadBytes);
        log.info("metadata: {}, payload: {}", metadata, string == null ? payloadBytes : string);
        return true;
    }

    @Override
    public List<Result> query(Query query) {
        return List.of();
    }

    @Override
    protected boolean init(Map<String, Object> pluginConfig) {
        return true;
    }

    private String fromUTF8(byte[] bytes) {
        ByteBuffer byteBuffer = ByteBuffer.wrap(bytes);
        try {
            decoder.decode(byteBuffer);
            return new String(bytes, StandardCharsets.UTF_8);
        } catch (CharacterCodingException e) {
            return null;
        }
    }
}
