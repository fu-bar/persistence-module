package osf.persistence.plugins;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import osf.persistence.config.Config;
import osf.persistence.plugins.base.OSFPersistencePlugin;
import osf.persistence.structures.Query;
import osf.persistence.structures.Result;
import osf.persistence.utilities.Utils;
import osf.shared.exceptions.OsfException;

import java.sql.*;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;

public class PostgresPlugin extends OSFPersistencePlugin {
    private String url;
    private String username;
    private String password;
    private String schemaName;
    private String tableName;

    public PostgresPlugin(Config configuration) {
        super(configuration);
    }

    @Override
    protected boolean init(Map<String, Object> pluginConfig) {
        this.url = (String) pluginConfig.get("url");
        this.schemaName = (String) pluginConfig.get("schemaName");
        this.tableName = (String) pluginConfig.get("tableName");
        this.username = (String) pluginConfig.get("username");
        this.password = (String) pluginConfig.get("password");

        String tableExistsSQL = String.format("SELECT EXISTS (\n" +
                "   SELECT FROM pg_tables\n" +
                "   WHERE  schemaname = '%s'\n" +
                "   AND    tablename  = '%s'\n" +
                "   )", schemaName, tableName);
        try (
                Connection connection = DriverManager.getConnection(url, username, password);
                Statement statement = connection.createStatement()
        ) {
            boolean tableExists = statement.execute(tableExistsSQL);
            if (!tableExists) {
                log.error("persistence.storage table not found");
                return false;
            }
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            return false;
        }
        return true;
    }

    @Override
    public boolean store(Map<String, Object> metadata, byte[] payloadBytes) {
        String metadataJson = Utils.toJson(metadata);
        String payloadBase64 = Base64.getEncoder().encodeToString(payloadBytes);
        String insertSQL = String.format(
                "INSERT INTO persistence.storage (metadata, payload) " +
                        "VALUES ('%s', decode('%s', 'base64'))", metadataJson, payloadBase64);
        try (
                Connection connection = DriverManager.getConnection(url, username, password);
                Statement statement = connection.createStatement()
        ) {
            statement.execute(insertSQL);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            return false;
        }
        return true;
    }

    @Override
    public List<Result> query(Query query) {
        List<Result> results = new ArrayList<>();
        try (
                Connection connection = DriverManager.getConnection(url, username, password);
                Statement statement = connection.createStatement()
        ) {
            Map<String, Object> metadata = query.getMetadata();
            String selectQuery = String.format("SELECT * FROM %s.%s WHERE %s",
                    this.schemaName,
                    this.tableName,
                    buildWhereClause(metadata)
            );
            ResultSet resultSet = statement.executeQuery(selectQuery);
            while (resultSet.next()) {
                String resultMetadata = resultSet.getString(3);
                byte[] resultPayload = resultSet.getBytes(4);
                results.add(new Result(Utils.OBJECT_MAPPER.readValue(resultMetadata, new TypeReference<>() {
                }), resultPayload));
            }
        } catch (SQLException | JsonProcessingException e) {
            log.error(e.getMessage(), e);
            throw new OsfException(e.getMessage(), e);
        }
        return results;
    }

    private String buildWhereClause(Map<String, Object> metadata) {
        List<String> whereConditions = new ArrayList<>();
        metadata.forEach((key, value) -> {
            String valueString = value.toString();
            if (isInteger(valueString)) {
                whereConditions.add(String.format("CAST (metadata->>'%s' AS INTEGER) = %d", key, Integer.parseInt(valueString)));
            } else if (isDouble(valueString)) {
                whereConditions.add(String.format("CAST (metadata->>'%s' AS DOUBLE PRECISION) = %f", key, Double.parseDouble(valueString)));
            } else {
                whereConditions.add(String.format("metadata->>'%s'='%s'", key, value));
            }
        });
        return String.join(" AND ", whereConditions);
    }

    private boolean isInteger(String string) {
        try {
            Integer.parseInt(string);
            return true;
        } catch (NumberFormatException numberFormatException) {
            return false;
        }
    }

    private boolean isDouble(String string) {
        try {
            Double.parseDouble(string);
            return true;
        } catch (NumberFormatException numberFormatException) {
            return false;
        }
    }
}
