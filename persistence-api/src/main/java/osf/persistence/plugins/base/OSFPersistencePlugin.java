package osf.persistence.plugins.base;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import osf.persistence.config.Config;
import osf.persistence.structures.Query;
import osf.persistence.structures.Result;

import java.util.List;
import java.util.Map;


public abstract class OSFPersistencePlugin {
    protected final Logger log;

    protected OSFPersistencePlugin(Config configuration) {
        this.log = LogManager.getLogger(this.getName());
        Map<String, Object> pluginConfig = configuration.getPluginConfig();
        boolean initSuccess = this.init(pluginConfig);
        if (!initSuccess) {
            log.error("Failed initializing persistence plugin {} with configuration: {}", this.getName(), pluginConfig);
            System.exit(-1);
        }
    }

    public String getName() {
        return this.getClass().getSimpleName();
    }

    /**
     * Initialize selected plugin with provided configurations
     *
     * @param pluginConfig Provided configurations
     * @return Whether initialization succeeded
     */
    protected abstract boolean init(Map<String, Object> pluginConfig);

    /**
     * Persist input
     *
     * @return Whether persisting was successful
     */
    public abstract boolean store(Map<String, Object> metadata, byte[] payloadBytes);


    public abstract List<Result> query(Query query);
}
