package osf.persistence.config;

import java.util.Map;

public class TCPConfig {
    private Class<?> persistencePluginClass;
    private Map<String, Object> pluginConfig;
    private int tcpServerPort;

    public Class<?> getPersistencePluginClass() {
        return persistencePluginClass;
    }

    public void setPersistencePluginClass(Class<?> persistencePluginClass) {
        this.persistencePluginClass = persistencePluginClass;
    }

    public Map<String, Object> getPluginConfig() {
        return pluginConfig;
    }

    public void setPluginConfig(Map<String, Object> pluginConfig) {
        this.pluginConfig = pluginConfig;
    }

    public int getTcpServerPort() {
        return tcpServerPort;
    }

    public void setTcpServerPort(int tcpServerPort) {
        this.tcpServerPort = tcpServerPort;
    }
}
