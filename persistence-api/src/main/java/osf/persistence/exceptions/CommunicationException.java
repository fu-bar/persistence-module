package osf.persistence.exceptions;

public class CommunicationException extends PersistenceException {
    public CommunicationException(String errorMessage) {
        super(errorMessage);
    }

    public CommunicationException(String errorMessage, Throwable cause) {
        super(errorMessage, cause);
    }
}
