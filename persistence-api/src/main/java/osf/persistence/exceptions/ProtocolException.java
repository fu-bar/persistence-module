package osf.persistence.exceptions;

public class ProtocolException extends CommunicationException {
    public ProtocolException(String errorMessage) {
        super(errorMessage);
    }

    public ProtocolException(String errorMessage, Throwable cause) {
        super(errorMessage, cause);
    }
}
