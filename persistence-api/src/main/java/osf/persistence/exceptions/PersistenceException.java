package osf.persistence.exceptions;

public class PersistenceException extends RuntimeException {
    public PersistenceException(String errorMessage) {
        super(errorMessage);
    }

    public PersistenceException(String errorMessage, Throwable cause) {
        super(errorMessage, cause);
    }
}
