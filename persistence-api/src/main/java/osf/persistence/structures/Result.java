package osf.persistence.structures;

import java.util.Arrays;
import java.util.Map;

public class Result {
    private final Map<String, Object> metadata;
    private final byte[] payload;

    public Result(Map<String, Object> metadata, byte[] payload) {
        this.metadata = Map.copyOf(metadata);
        this.payload = Arrays.copyOf(payload, payload.length);
    }

    public Map<String, Object> getMetadata() {
        return metadata;
    }

    public byte[] getPayload() {
        return payload;
    }
}
