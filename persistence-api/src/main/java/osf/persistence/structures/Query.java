package osf.persistence.structures;

import java.time.Instant;
import java.util.Map;

public class Query {
    private final Instant timestamp;
    private final Map<String, Object> metadata;
    private Instant minTimestamp = Instant.MIN;
    private Instant maxTimestamp = Instant.MAX;

    public Query(Instant minTimestamp, Instant maxTimestamp, Map<String, Object> metadata) {
        this.timestamp = Instant.now();
        this.metadata = metadata;
        if (minTimestamp != null) {
            this.minTimestamp = minTimestamp;
        }
        if (maxTimestamp != null) {
            this.maxTimestamp = maxTimestamp;
        }

    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public Instant getMinTimestamp() {
        return minTimestamp;
    }

    public Instant getMaxTimestamp() {
        return maxTimestamp;
    }

    public Map<String, Object> getMetadata() {
        return metadata;
    }
}
